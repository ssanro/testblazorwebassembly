using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TestBlazor.Configuration;
using TestBlazor.Services;

namespace TestBlazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            // Register app helpers
            builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthStateProvider>();

            // Register app services
            builder.Services.AddScoped<IAccountService, AccountService>();
            builder.Services.AddScoped<IAuthApiService, AuthApiService>();

            // Register external services
            builder.Services.AddAuthorizationCore();
            builder.Services.AddBlazoredLocalStorage();

            await builder.Build().RunAsync();
        }
    }
}
