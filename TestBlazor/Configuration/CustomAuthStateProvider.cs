﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;
using TestBlazor.Helpers;
using TestBlazor.Models;

namespace TestBlazor.Configuration
{
    public class CustomAuthStateProvider : AuthenticationStateProvider
    {
		private readonly ILocalStorageService _localStorageService;

		public CustomAuthStateProvider(ILocalStorageService localStorageService)
		{
			_localStorageService = localStorageService;
		}

		/// <summary>
		/// Gets token from local storage
		/// </summary>
		/// <returns></returns>
		public override async Task<AuthenticationState> GetAuthenticationStateAsync()
		{
			var token = await _localStorageService.GetItemAsync<string>("token");
			
			if (string.IsNullOrEmpty(token))
			{
				var anonymous = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));
				return anonymous;
			}

			var user = TokenService.FromBase64<User>(token);
			var userClaimPrincipal = new ClaimsPrincipal(TokenService.ParseClaims(user));
			var loginUser = new AuthenticationState(userClaimPrincipal);

			return loginUser;
		}

		/// <summary>
		/// Notify change.
		/// </summary>
		public void Notify()
		{
			NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
		}
	}
}
