﻿using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Text;
using TestBlazor.Models;

namespace TestBlazor.Helpers
{
    public static class TokenService
    {
        /// <summary>
        /// Sets user claims
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static ClaimsIdentity ParseClaims(User user)
        {
            var identity = new ClaimsIdentity();

            if (user != null){
                identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.FirstName),
                    new Claim(ClaimTypes.Surname, user.LastName),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.HomePhone, user.PhoneNumber)
                }, "Fake Authentication");
            }

            return identity;
        }

        /// <summary>
        /// Transform object to B64
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToBase64(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            var bytes = Encoding.Default.GetBytes(json);

            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Gets object form B64
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="base64Text"></param>
        /// <returns></returns>
        public static T FromBase64<T>(this string base64Text)
        {
            var bytes = Convert.FromBase64String(base64Text);
            var json = Encoding.Default.GetString(bytes);

            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
