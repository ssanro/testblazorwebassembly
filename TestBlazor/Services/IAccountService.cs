﻿using System.Threading.Tasks;
using TestBlazor.Models;

namespace TestBlazor.Services
{
    public interface IAccountService
    {
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> LoginAsync(LoginModel model);

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        Task<bool> LogoutAsync();
    }
}
