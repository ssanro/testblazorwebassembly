﻿using System.Collections.Generic;
using System.Linq;
using TestBlazor.Models;

namespace TestBlazor.Services
{
    public class AuthApiService : IAuthApiService
    {
        /// <summary>
        /// Mock user list
        /// </summary>
        private readonly List<User> Users = new()
        {
            new User{
                Id = 1,
                FirstName = "admin",
                LastName = "web",
                Email = "admin@admin.com",
                Password = "1234",
                PhoneNumber = "69000000"
            }
        };

        /// <summary>
        /// Gets authentication token
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        public User GetAuthenticationToken(LoginModel loginModel)
        {
            User currentUser = Users.FirstOrDefault(_ => _.Email.ToLower() == loginModel.Email.ToLower() && _.Password == loginModel.Password);

            if (currentUser != null) return currentUser;

            return null;
        }
    }
}
