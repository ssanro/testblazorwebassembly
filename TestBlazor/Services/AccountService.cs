﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;
using TestBlazor.Configuration;
using TestBlazor.Helpers;
using TestBlazor.Models;

namespace TestBlazor.Services
{
    public class AccountService : IAccountService
    {
		private readonly AuthenticationStateProvider _customAuthenticationProvider;
		private readonly ILocalStorageService _localStorageService;
		private readonly IAuthApiService _authApiService;

		public AccountService(ILocalStorageService localStorageService, AuthenticationStateProvider customAuthenticationProvider,
							  IAuthApiService authApiService)
		{
			_localStorageService = localStorageService;
			_customAuthenticationProvider = customAuthenticationProvider;
			_authApiService = authApiService;
		}

		/// <summary>
		/// Login
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public async Task<bool> LoginAsync(LoginModel model)
		{
			if (string.IsNullOrEmpty(model.Email)) return false;

			var response = _authApiService.GetAuthenticationToken(model);

			if (response == null) return false;

			await _localStorageService.SetItemAsync("token", TokenService.ToBase64(response));
			(_customAuthenticationProvider as CustomAuthStateProvider).Notify();
			
			return true;
		}

		/// <summary>
		/// Logout
		/// </summary>
		/// <returns></returns>
		public async Task<bool> LogoutAsync()
		{
			await _localStorageService.RemoveItemAsync("token");
			(_customAuthenticationProvider as CustomAuthStateProvider).Notify();

			return true;
		}

	}
}
