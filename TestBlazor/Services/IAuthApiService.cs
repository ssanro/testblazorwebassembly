﻿using TestBlazor.Models;

namespace TestBlazor.Services
{
    public interface IAuthApiService
    {
        /// <summary>
        /// Gets authentication token
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        User GetAuthenticationToken(LoginModel loginModel);
    }
}
